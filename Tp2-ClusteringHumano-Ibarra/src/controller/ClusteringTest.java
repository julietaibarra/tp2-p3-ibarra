package controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import model.Assert;

import model.Person;

public class ClusteringTest {
	Person a, b, c,d,e;
	ArrayList<Person> persons;

	@Before 
	   public void initialize() {
		a= new Person("A", 2, 3, 1,5);
		b= new Person("B",5,5,1,1);   
		c= new Person("C", 5,5,1,1);
		d= new Person("D",3,5,4,2);
		e= new Person("E", 3,3,3,3);
		persons= new ArrayList<Person>();
		persons.add(a);
		persons.add(b);
		persons.add(c);
		persons.add(d);
		persons.add(e);
	}
	
	@Test
	public void dosGruposTest() {
		Clustering cluster= new Clustering(persons);
		Person [] esperado= {e,d,a};
		cluster.generateGroups(2);

		Assert.iguales(esperado, cluster.getGroup(a));
	}
	
	@Test
	public void ReturnGruposTest() {
		Clustering cluster= new Clustering(persons);
		Person [][]esperado= {{c,b},{e,d,a}};
		cluster.generateGroups(2);
		Assert.iguales(esperado, cluster.returnGroup());
	}
	@Test
	public void avergeTest() {
		Clustering cluster= new Clustering(persons);
		cluster.generateGroups(2);
		double [][] expected= {{5.0,5.0,1.0,1.0},
								{2.67,3.67,2.67,3.33}};

		double [][]averge=cluster.averageSimilarity();
		Assert.iguales(expected,averge);
		
	}

	@Test (expected = RuntimeException.class)
	public void unGrupoTest() {
		Clustering cluster= new Clustering(persons);
		cluster.generateGroups(1);
	}
	
	@Test (expected = RuntimeException.class)
	public void cincoGruposTest() {
		Clustering cluster= new Clustering(persons);	
		cluster.generateGroups(5);		
	}	
}