package controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import model.BFS;
import model.Graph;
import model.Kruskal;
import model.Person;


public class Clustering {
	private ArrayList<Person> inputData;
	private Graph graph;
	private Kruskal kruskal;
	private static Graph aux ;
	private HashSet<Set<Person>> groups;

	public Clustering(ArrayList<Person> input) {
		inputData = new ArrayList<>();
		inputData = input;
		graph = new Graph(inputData);
		kruskal = new Kruskal(graph);	
		groups = new HashSet<Set<Person>>();
		aux = new Graph();
		aux = kruskal.minimalGeneratorTree();
	}
	
	private void cluster() {
		aux.removeEdge(aux.maxEdge().getPerson1(), aux.maxEdge().getPerson2());
	}
	
	private void addGroups() {
		for (Person person : graph.getPersons()) {
			groups.add(getGroup(person));
		}
	}
	
	public Set<Person> getGroup(Person person){
		return BFS.achievable(aux, person);
	}

	public void generateGroups(int cant) {
		if (cant>1 && cant<5) {
			for (int i = 0; i < cant-1; i++) {
				cluster();
			}
		}else
			throw new RuntimeException("valor invalido");
	}
	
	public HashSet<Set<Person>> returnGroup(){
		addGroups();
		return groups;
	}
	
	public double[][] averageSimilarity() {
		addGroups();
		double [][] avergeSimirality= new double[groups.size()][4];
		int index=0;
		for (Set<Person> set : groups) {	
			for (Person persona: set) {
				avergeSimirality[index][0]+=persona.getInterestSport();
				avergeSimirality[index][1]+=persona.getInterestMusic();
				avergeSimirality[index][2]+=persona.getInterestShow();
				avergeSimirality[index][3]+=persona.getInterestScience();
			}
			avergeSimirality[index][0]=(double)Math.round(avergeSimirality[index][0]/set.size() * 100d) / 100d;	
			avergeSimirality[index][1]=(double)Math.round(avergeSimirality[index][1]/set.size() * 100d) / 100d;
			avergeSimirality[index][2]=(double)Math.round(avergeSimirality[index][2]/set.size() * 100d) / 100d;
			avergeSimirality[index][3]=(double)Math.round(avergeSimirality[index][3]/set.size() * 100d) / 100d;
			index++;
		}
		return avergeSimirality;
	}
}