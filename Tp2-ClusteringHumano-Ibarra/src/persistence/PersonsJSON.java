package persistence;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.Person;

public class PersonsJSON {
	private ArrayList<Person> persons;
	
	public PersonsJSON() {
		persons = new ArrayList<Person>();
	}
	
	public void addPerson(Person p) {
		persons.add(p);
	}
	
	public Person get(int index) {
		return persons.get(index);
	}
	
	public int size() {
		return persons.size();
	}

	public void saveJSON(String jsonForSave, String destinyFile) {
		try {
			FileWriter writer= new FileWriter(destinyFile);
			writer.write(jsonForSave);
			writer.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public PersonsJSON readJSON(String file) {
		Gson gson = new Gson();
		PersonsJSON ret= null;
		try {
			BufferedReader buffer= new BufferedReader(new FileReader(file));
			ret = gson.fromJson(buffer, PersonsJSON.class);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}
}
