package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Graph {
	private HashMap<Person, HashSet<Person>> neighbors;
	private ArrayList<Edge>  edges;
	private ArrayList<Person> persons;
	private int cantPersons;
	
	public Graph(int cantP) {
		cantPersons=cantP;
		edges=new ArrayList<Edge>();
		neighbors=new HashMap<Person, HashSet<Person>>(cantPersons);
		persons= new ArrayList<Person>();
	}

	public Graph() {
		edges=new ArrayList<Edge>();
		neighbors=new HashMap<Person, HashSet<Person>>();
		cantPersons=neighbors.size();
		persons= new ArrayList<Person>();
	}

	public Graph(ArrayList<Edge> e, int cantP) {
		edges=new ArrayList<Edge>();
		persons= new ArrayList<Person>();
		neighbors=new HashMap<Person, HashSet<Person>>();
		for (Edge edge : e) {
			addEdge(edge.getPerson1(), edge.getPerson2());
		}
		cantPersons=cantP;
	}
	
	public Graph( ArrayList<Person> p) {
		edges=new ArrayList<Edge>();
		persons= new ArrayList<>();
		neighbors=new HashMap<Person, HashSet<Person>>();
		for (Person person : p) {
			addPerson(person);		
		}
		cantPersons=neighbors.size();
	}
//---------------------------------------------------------------
	private void addToPersons(Person p) {
		if (!persons.contains(p)) {
			persons.add(p);
			p.setId(indexPerson(p));
		}
	}
	
	public void addPerson(Person p) {
		if (persons.size()==0 ) {
			neighbors.put(p, new HashSet<Person>());
			addToPersons(p);
			
		}else {
			for (int i = 0; i < PersonsSize(); i++) {
				if (!persons.get(i).equals(p)) {
					addEdge(persons.get(i), p);
				}
			}
		
		}
	}
	
	void addEdge(Person p1, Person p2) {
		checkDifferent(p1,p2);
		if (!existsEdge(p1, p2)&&!existsEdge(p2, p1)){
		addneighbor(p1, p2);
		addneighbor(p2, p1);
		edges.add(new Edge(p1, p2));
	    addToPersons(p1);
	    addToPersons(p2);
		}
		
	}

	private void addneighbor(Person p1, Person p2) {
		if(!neighbors.containsKey(p1)) {
			neighbors.put(p1, new HashSet<Person>());
			neighbors.get(p1).add(p2);
		}else {
			neighbors.get(p1).add(p2);
		}
	}
	
//---------------------------------------------------------------
	public void removePerson(Person p) {
		if (PersonExists(p)) {
			for (int i = 0; i < PersonsSize(); i++) {
				if (!persons.get(i).equals(p)) {
					removeEdge(persons.get(i), p);
				}
			}
			persons.remove(p);
		}
	}

	public void removeEdge(Person p1, Person p2) {
		checkDifferent(p1, p2);
		Edge aux1=new Edge(p1,p2);
		if(existsEdge(p1,p2)){
			edges.remove(aux1);
			removeNeighbor(p1, p2);
			removeNeighbor(p2, p1);
		}
	}
	
	private void removeNeighbor(Person key, Person p) {
		if (neighbors.get(key).contains(p)) {
				neighbors.get(key).remove(p);
		}
}
//---------------------------------------------------------------
	public Set<Person> neighbors(Person p){
		Set<Person> ret= new HashSet<Person>();
		if(PersonExists(p)) {
			ret=neighbors.get(p);
		}
		return ret;
	}
	public ArrayList<Person> getPersons() {
		return persons;
	}
	
	public ArrayList<Edge> getEdges(){
		return edges;
	}
	
	public Person getPerson(int i) {
		return persons.get(i);
	}
	
	public Edge getEdge(int i) {
		return edges.get(i);
	}
	
	public Edge maxEdge() {
		return Collections.max(getEdges()) ;
	}
//---------------------------------------------------------------
	public int cantEdges() {
		return edges.size();		
		}
	
	public int Persons() {
		//returns the entered value 
		return cantPersons;
	}
	
	public int PersonsSize() {
		//return the side of the array
		return persons.size();
	}

	public int indexPerson(Person p) {
		return persons.indexOf(p);
	}
	
	public int cantNeighbors(Person p) {
		return neighbors.get(p).size();
	}
//---------------------------------------------------------------
	public boolean PersonExists(Person p) {
			return neighbors.containsKey(p);
		}

	public boolean existsEdge(Person p1, Person p2) {
			return edges.contains(new Edge(p1,p2));
		}

	private void checkDifferent(Person p1, Person p2) {
		if (p1.equals(p2))
			throw new IllegalArgumentException("No se permiten loops ");
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean ret1, ret2, ret3, ret4;
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Graph other = (Graph) obj;
		if (edges == null) {
			if (other.edges != null)
				return false;
		} else{
			ret1 = true;
			ret2= true;
			Iterator<Edge> it=edges.iterator();
			Iterator<Edge> it2=other.edges.iterator();
			while (it.hasNext()) {
				Edge Edge1 = (Edge) it.next();
				Edge Edge2=(Edge) it2.next();
				ret1=ret1&& edges.contains(Edge2);
				ret2=ret2&&other.edges.contains(Edge1);
			}
			return ret2 && ret1;
		}
		if (neighbors == null) {
			if (other.neighbors != null)
				return false;
		} else if (!neighbors.equals(other.neighbors))
			return false;
		if (persons == null) {
			if (other.persons != null)
				return false;
		} else {
			ret3=true;
			ret4= true;
			Iterator<Person> it=persons.iterator();
			Iterator<Person> it2=other.persons.iterator();
			while (it2.hasNext()) {
				Person Person1 = (Person) it.next();
				Person Person2 = (Person) it2.next();
				ret3=ret3&&persons.contains(Person2);
				ret4=ret4&&other.persons.contains(Person1);
			}
			return ret3&&ret4;	
		}
		return true;
	}
}