package model;

import static org.junit.Assert.*;


import java.util.Set;

import org.junit.Before;
import org.junit.Test;


public class BFSTest {
	Person a, b, c,d, e;
	
	@Before 
	   public void initialize() {
		a= new Person("A", 2, 3, 1, 5);
		b= new Person("B",5,5,1,1);   
		c= new Person("C", 5,5,1,1);
		d= new Person("D",3,5,4,2);
		e= new Person("E", 3,3,3,3);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testNull() {
		BFS.isConnectedGraph(null);
		
	}
	
	@Test
	public void emptyTest() {
		Graph graph= new Graph(0);
		
		assertTrue(BFS.isConnectedGraph(graph));
	}
	
	@Test
	public void achievableTest() {
		Graph graph= initializeGrph();
		 Set<Person> achievable =BFS.achievable(graph, a);
		Person [] expected = {a,b,c,d};
		
		Assert.iguales(expected, achievable);
		
	}
	@Test
	public void conexoTest() {//modificar despues
		Graph graph= initializeGrph();
		
		graph.addEdge(d, e);
		
		assertTrue(BFS.isConnectedGraph(graph));
		
	}

	@Test
	public void noConexoTest() {
		Graph graph= initializeGrph();
		assertFalse(BFS.isConnectedGraph(graph));
	}

	private Graph initializeGrph() {
		Graph graph= new Graph(5);
		graph.addEdge(a, b);
		graph.addEdge(a, c);
		graph.addEdge(c, d);
		
		return graph;
	}
}