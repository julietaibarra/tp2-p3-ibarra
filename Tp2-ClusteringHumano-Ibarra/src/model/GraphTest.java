package model;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

public class GraphTest {
	private Person a, b, c,d,e;
	private ArrayList<Person> persons;
	private ArrayList<Edge> edges;
	   
	@Before 
	   public void initialize() {
		a= new Person("A", 2, 3, 1, 5);
		b= new Person("B",5,5,1,1);   
		c= new Person("C", 5,5,1,1);
		d= new Person("D",3,5,4,2);
		e= new Person("E", 3,3,3,3);
		
		persons= new ArrayList<Person>();
		persons.add(a);
		persons.add(b);
		persons.add(c);
		persons.add(d);
		persons.add(e);
		edges = new ArrayList<Edge>();
		
		edges.add(new Edge(a, b));
		edges.add(new Edge(b, c));
		edges.add(new Edge(c, a));
	}
	
	@Test
	public void cantidadAristaTest() {
		Graph graph= graph1();
		assertEquals(2, graph.cantEdges());
	}
	
	@Test
	public void cantidadAristaTest2() {
		Graph graph= graph3();
		assertEquals(15, graph.cantEdges());
	}
	@Test
	public void cantidadAristaTest3() {
		Graph graph= new Graph();
		
		graph.addPerson(a);
		assertEquals(0, graph.cantEdges());

		graph.addPerson(b);
		assertEquals(1, graph.cantEdges());
	}
	
	@Test
	public void getEdgesTest() {
		Graph graph= graph1();
		Edge edge= new Edge(a,b);
		assertTrue(graph.getEdge(0).equals(edge));
	}
	
	@Test
	public void removeEdgeTest() {
		Graph graph=graph2();
		
		graph.removeEdge(a,c);
		graph.removeEdge(c, b);
		
		assertEquals(1, graph.cantEdges());
	}
	
	//add per person and still create a complete graph
	
	@Test
	public void addPerson() {
		Graph graph=graph2();		
		graph.addPerson(new Person("Ana", 1, 5, 4, 3));
		assertEquals(6, graph.cantEdges());
	}
	
	@Test
	public void addEdges() {
		//add arraylist of p
		Graph graph=new Graph(edges, 4);
	
		assertEquals(3, graph.cantEdges());
		graph.addEdge(a, d);
		assertEquals(4, graph.cantEdges());
	}
	
	@Test
	public void addPersons() {
		//agregar arraylist de p
		Graph graph=new Graph(persons);
	
		assertEquals(10, graph.cantEdges());
		
	}
	@Test
	public void indexTest() {
		Graph graph=graph2();
		
		graph.addEdge(d,e);
		
		assertTrue(graph.indexPerson(d)==3);
	}
	
	@Test
	public void existEdge() {
		Graph graph= graph2();
	
		assertTrue(graph.existsEdge(c, b));
	
		graph.removeEdge(c, b);
		
		assertFalse(graph.existsEdge(new Person("A", 1, 3, 3, 4), b));
		assertFalse(graph.existsEdge(c, b));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void addLoopsTest() {
		Graph graph= new Graph(5);
		graph.addEdge(a, a);;
	}

	@Test
	public void getPersonsTest() {
		Graph graph= graph3();
		persons.add(new Person("Ana", 1, 5, 4, 3));

		assertEquals(persons, graph.getPersons());

	}
	
///-------------------------------------------------	
	private Graph graph1() {
		Graph graph= new Graph();
		
		graph.addEdge(a,b );
		graph.addEdge(c, e);
		
		return graph;
	}
	private Graph graph2() {
		Graph graph= new Graph();
		
		graph.addEdge(a,b);
		graph.addEdge(c,b);
		graph.addEdge(a,c);
		
		return graph;
	}
	
		private Graph graph3() {
		Graph graph= new Graph(persons);
		graph.addPerson(new Person("Ana", 1, 5, 4, 3));

		return graph;
	}

}
