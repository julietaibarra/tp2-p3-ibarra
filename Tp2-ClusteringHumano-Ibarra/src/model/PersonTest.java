package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest {
		private Person person;
		
	@Test (expected = RuntimeException.class)
	public void personNameEmptyTest() {
		person= new Person("", 4, 2, 5, 1);
	}
	
	@Test (expected = RuntimeException.class)
	public void negativeNumberTest() {
		 person= new Person("ana", 5, -1, 5, 1);
		
	}
	@Test (expected = RuntimeException.class)
	public void numberCeroTest() {
		 person= new Person("ana", 4, 2, 5, 0);
		
	}
	@Test (expected = RuntimeException.class)
	public void numberGreaterThanFiveTest() {
		 person= new Person("ana", 4, 2, 6, 1);
		
	}
	@Test
	public void PersonConstructorTest() {
		 person= new Person("ana", 4, 2, 5, 1);
		
		assertTrue(person.getName().equals("ana"));
	}
}
