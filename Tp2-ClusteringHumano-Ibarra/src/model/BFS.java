package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BFS {
	private static boolean [] marked;
	private static ArrayList<Person> L;
	
	
	public static boolean isConnectedGraph(Graph g) {
		if(g==null) {
			throw new IllegalArgumentException("Se intento consultar un Graph que es null");

		}
		if(g.Persons()==0) {
			return true;
		}
		return achievable(g, g.getPerson(0)).size()==g.Persons();
			
	}
	
	
	
	public static Set<Person> achievable(Graph g, Person origin){
		Set<Person> ret= new HashSet<Person>();
		inicialize(g, origin);
		
		while(L.size()>0) {
			Person i=L.get(0);
			marked[g.indexPerson(i)]=true;
			ret.add(i);
			
			addPendingNeightbors(g, i);
			L.remove(0);
			
		}
		return ret;
	}
	
	
	
	private static void addPendingNeightbors(Graph g, Person i) {

		for (Person Person : g.neighbors(i)) {
		if (marked[g.indexPerson(Person)] == false && !L.contains(Person)) {
			L.add(Person);
		
		}
		
		}	
	}
	
	private static void inicialize(Graph g, Person origin) {
		L = new ArrayList<Person>();
		L.add(origin);
		marked= new boolean [g.Persons()];
	}

}
