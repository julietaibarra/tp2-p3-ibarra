package model;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

public class EdgeTest {
	private Person a, b;
	   
	@Before 
	   public void initialize() {
		a= new Person("A", 2, 3, 1, 5);
		b= new Person("B",5,5,1,1);   
	}
	@Test
	public void similarityTest() {
		Edge edge= new Edge(a, b);
		assertTrue(edge.getLength()==9);
	}
	
	@Test
	public void equalsTest() {
		Edge edge= new Edge(a, b);
		assertFalse(edge.equals(null));
		assertTrue(edge.equals(new Edge(b,a)));
	}

}