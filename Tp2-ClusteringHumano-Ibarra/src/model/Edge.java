package model;

public class Edge implements Comparable<Edge>{
	private Person person1;
	private Person person2;
	private Integer length;

	public Edge(Person p1, Person p2) {
		person1=p1;
		person2= p2;
		length=similarity(person1, person2);
	}

	private Integer similarity(Person p1, Person p2) {
		return Math.abs(p1.getInterestSport()-p2.getInterestSport())+
			   Math.abs(p1.getInterestMusic()-p2.getInterestMusic())+
			   Math.abs(p1.getInterestShow() -p2.getInterestShow()) +
			   Math.abs(p1.getInterestScience()-p2.getInterestScience());
	}

	public Integer getLength() {
		return length;
	}
	public Person getPerson1() {
		return person1;
	}

	public Person getPerson2() {
		return person2;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (person1 == null) {
			if (other.person1 != null)
				return false;
		} else if (!person1.equals(other.person1)&&!person1.equals(other.person2))
			return false;
		if (person2 == null) {
			if (other.person2 != null)
				return false;
		} else if (!person2.equals(other.person2)&& !person2.equals(other.person1))
			return false;
		if (length == null) {
			if (other.length != null)
				return false;
		} else if (!length.equals(other.length))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Edge o) {
		if(getLength()<o.getLength())
				return -1;
		else if (getLength()==o.getLength())
			return 0;
		else
			return 1;
	}
}