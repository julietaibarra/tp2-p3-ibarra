package model;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

public class Assert {

	//verifica que sean iguales como conjuntos
		public static  void iguales(Person[] esperado, Set<Person> obtenido) {
			assertEquals(esperado.length , obtenido.size());
			for (int i = 0; i < esperado.length; i++) {
				assertTrue(obtenido.contains(esperado[i]));
			}
		}
		
		public static  void iguales(double[][] esperado, double[][] obtenido) {
			boolean ret=false;
			assertEquals(esperado.length , obtenido.length);
			assertEquals(esperado[0].length , obtenido[0].length);
			for (int i = 0; i < esperado.length; i++) {
				for (int j = 0; j < esperado[0].length; j++) {
					ret= ret|| esperado[i][j] ==obtenido[i][j];
					
				}
			}
			assertTrue(ret);
		}

		public static void iguales(Person[][] esperado, HashSet<Set<Person>> obtenido) {
			assertEquals(esperado.length , obtenido.size());
			int i=0;
			for (Set<Person> set : obtenido) {
				for (int j = 0; j < set.size(); j++) {
					assertTrue(set.contains(esperado[i][j]));
				}
				i++;
			}
		}
}