package model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

public class NeighborsTest {
	private Person a, b, c, d;
	private ArrayList<Person> persons;
	private Graph graph;
	
	@Before 
	   public void initialize() {
		a= new Person("A", 2, 3, 1, 5);
		b= new Person("B",5,5,1,1);   
		c= new Person("C", 5,5,1,1);
		d= new Person("D", 3, 3, 3, 4);
		persons= new ArrayList<Person>();
		persons.add(a);
		persons.add(b);
		persons.add(c);
		graph = new Graph(persons);
	}

	@Test
	public void neighborsCtest() {
		Person neigborsC[]= {b,a};
		Assert.iguales(neigborsC, graph.neighbors(c));
	}
	
	@Test
	public void removeNeighborTest() {
		graph.removePerson(a);
		Person neigborsB[]= {c};
		Assert.iguales(neigborsB, graph.neighbors(b));
	}
	
	@Test
	public void addNeighborTest() {
		graph.addPerson(d);
		Person neigborsB[]= {a,c,d};
		Assert.iguales(neigborsB, graph.neighbors(b));
	}
	@Test
	public void cantNeighborTest() {
		graph.addPerson(d);
		assertEquals(3, graph.cantNeighbors(b));
	}
}
