package model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class KruskalTest {
	Person a, b, c,d,e;
	ArrayList<Person> persons;
   
	@Before 
	   public void initialize() {
		a= new Person("A", 2, 3, 1,5);
		b= new Person("B",5,5,1,1);   
		c= new Person("C", 5,5,1,1);
		d= new Person("D",3,5,4,2);
		e= new Person("E", 3,3,3,3);
		
		persons= new ArrayList<Person>();
		persons.add(a);
		persons.add(b);
		persons.add(c);
		persons.add(d);
		persons.add(e);
	}
	
	@Test
	public void exapleTest() {
		Graph graph= new Graph(persons);
		Kruskal kruskal=new Kruskal(graph);
		assertEquals(expected(), kruskal.minimalGeneratorTree());
	}
	
	@Test
	public void maxEdgeTest() {
		Graph graph= new Graph(persons);
		Kruskal kruskal=new Kruskal(graph);
		Edge expected= new Edge(d, b);//weight 6
		assertEquals(expected, kruskal.minimalGeneratorTree().maxEdge());
	}
	
	private Graph expected() {
		Graph graph= new Graph(5);
		graph.addEdge(a, e);
		graph.addEdge(b, c);
		graph.addEdge(b, d);
		graph.addEdge(d, e);
		
		return graph;
	}
}