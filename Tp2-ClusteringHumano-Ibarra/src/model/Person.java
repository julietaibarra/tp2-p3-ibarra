package model;

public class Person {
	
	private String name;
	private Integer interestSport;
	private Integer interestMusic;
	private Integer interestShow;
	private Integer interestScience;
	private int id;
	
	public Person(String n, Integer sport, Integer music,Integer show, Integer science) {
		//determine the representation invariance
		if (!n.equals("") &&
			!(sport<1 || sport>5) &&
			!(music<1 || music>5) &&
			!(show<1  || show>5)  &&
			!(science<1 || science>5)) {
			name=n;
			interestSport=sport;
			interestMusic= music;
			interestShow= show;
			interestScience= science;
			id=0;
			
		}else {
			throw new RuntimeException("Datos ingrasados invalidos");
		}
		
	}
	
	public String getName() {
		return name;
	}

	public Integer getInterestSport() {
		return interestSport;
	}

	public Integer getInterestMusic() {
		return interestMusic;
	}

	public Integer getInterestShow() {
		return interestShow;
	}

	public Integer getInterestScience() {
		return interestScience;
	}

	public int getId() {
		return id;
	}

	public void setId(int number) {
		this.id = number;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((interestMusic == null) ? 0 : interestMusic.hashCode());
		result = prime * result + ((interestScience == null) ? 0 : interestScience.hashCode());
		result = prime * result + ((interestShow == null) ? 0 : interestShow.hashCode());
		result = prime * result + ((interestSport == null) ? 0 : interestSport.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (interestMusic == null) {
			if (other.interestMusic != null)
				return false;
		} else if (!interestMusic.equals(other.interestMusic))
			return false;
		if (interestScience == null) {
			if (other.interestScience != null)
				return false;
		} else if (!interestScience.equals(other.interestScience))
			return false;
		if (interestShow == null) {
			if (other.interestShow != null)
				return false;
		} else if (!interestShow.equals(other.interestShow))
			return false;
		if (interestSport == null) {
			if (other.interestSport != null)
				return false;
		} else if (!interestSport.equals(other.interestSport))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return   name  ;
	}
}
