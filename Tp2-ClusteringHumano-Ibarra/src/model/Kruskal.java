package model;

import java.util.ArrayList;
import java.util.Collections;

public class Kruskal {
	private  int[]link;
	private Graph graph;
	private ArrayList<Edge> listEdge;
	
	public Kruskal(Graph g) {
		graph=g;
		listEdge= new ArrayList<Edge>();
		listEdge=g.getEdges();
	}
	
	private void initRoot() {
		link= new int[graph.Persons()];
		for (int j = 0; j < link.length; j++) {
			link[j]=j;
		}
	}
	
	//Determine if two persons are in the same connected component
	private int find(int i) {
		if (i== link[i]) {
			return i;
		}
		return link[i]= find(link[i]);
	}
	
	//Modify the structure of the trees
	private void union(int i, int j) {
		int rootI=find(i);
		int rootJ=find(j);
		link[rootI]=rootJ;
	}
	
	public Graph minimalGeneratorTree() {
		Collections.sort(listEdge);
		ArrayList<Edge> aux=new ArrayList<>();
		initRoot();
		int connectedComponent= graph.Persons();
		int position=0;
		while ((connectedComponent!=1 && position < graph.cantEdges())) {
			Edge current= listEdge.get(position);
			if (find(current.getPerson1().getId())!=find(current.getPerson2().getId())) {
				union(current.getPerson1().getId(), current.getPerson2().getId());
				aux.add(current);
				connectedComponent --;
			}
			 position++;
		}
		return new Graph(aux, graph.Persons());
	}
}