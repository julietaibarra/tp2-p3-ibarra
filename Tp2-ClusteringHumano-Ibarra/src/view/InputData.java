package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import model.Person;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Font;

	public class InputData extends JFrame {                    
	    private JButton btnAdd, btnReady;
	    private JPanel jPanel1, jPanel2;
	    private JScrollPane jScrollPane1;
	    private JTable dataTable;
	    private JTextField userName;
	    private JLabel lblSport, lblMusic, lblName;
	    private JComboBox<Integer> comboSport,comboMusic, comboShow , comboScience ;
        private ArrayList<Person> persons;
        private JLabel lblImg ,lblShow,lblScience ;

	    public InputData() {
	    	getContentPane().setBackground(new Color(102, 205, 170));
	        initComponents();
	        this.setLocationRelativeTo(null);  
	    }
	    
	    private void initComponents() {
	        jPanel1 = new JPanel();
	        lblName = new JLabel();
	        lblSport = new JLabel();
	        lblMusic = new JLabel();
	        userName = new JTextField();
	        btnAdd = new JButton();
	        btnAdd.setForeground(new Color(245, 255, 250));
	        btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 13));
	        btnAdd.setBackground(new Color(32, 178, 170));
	        jPanel2 = new JPanel();
	        jScrollPane1 = new JScrollPane();
	        dataTable = new JTable();
	        persons= new ArrayList<Person>();
	        
	        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	        addWindowListener(new WindowAdapter() {
	            public void windowOpened(WindowEvent evt) {
	            	Tools.loadTable(evt, dataTable);
	            }
	        });
	        jPanel1.setBorder(new TitledBorder(null, "Ingreso de datos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
	        jPanel1.setBackground(new Color(153, 218, 182));
	        lblName.setText("Nombre");
	        lblSport.setText("Deporte");
	        lblMusic.setText("M�sica");
	        lblShow = new JLabel("Espectaculos");	        
	        lblScience = new JLabel("Ciencia");
	        comboSport = new JComboBox<Integer>();
	       Tools.optionsBox(comboSport,1,5);	        
	      	comboMusic = new JComboBox<Integer>();
	      	Tools.optionsBox(comboMusic,1,5);	        
	        comboShow = new JComboBox<Integer>();
	        Tools.optionsBox(comboShow,1,5);        
	         comboScience = new JComboBox<Integer>();
	        Tools.optionsBox(comboScience,1,5);
	        btnAdd.setText("agregar");
	        btnAdd.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent evt) {
	                loadDataToTable(evt);
	            }
	        });	        
	        lblImg = new JLabel("");
	        lblImg.setIcon(new ImageIcon("src/groups2.png"));	        
	        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
	        jPanel1Layout.setHorizontalGroup(
	        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
	        		.addGroup(jPanel1Layout.createSequentialGroup()
	        			.addGap(30)
	        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
	        				.addGroup(jPanel1Layout.createSequentialGroup()
	        					.addComponent(lblMusic)
	        					.addContainerGap())
	        				.addGroup(jPanel1Layout.createSequentialGroup()
	        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
	        						.addComponent(lblSport)
	        						.addComponent(lblName))
	        					.addPreferredGap(ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
	        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
	        						.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
	        							.addComponent(comboSport, 0, 76, Short.MAX_VALUE)
	        							.addComponent(comboMusic, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	        							.addComponent(comboShow, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	        							.addComponent(comboScience, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	        							.addComponent(btnAdd, Alignment.TRAILING))
	        						.addGroup(jPanel1Layout.createSequentialGroup()
	        							.addComponent(userName, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
	        							.addGap(10)))
	        					.addGap(21))))
	        		.addGroup(jPanel1Layout.createSequentialGroup()
	        			.addGap(21)
	        			.addComponent(lblShow)
	        			.addContainerGap(180, Short.MAX_VALUE))
	        		.addGroup(jPanel1Layout.createSequentialGroup()
	        			.addGap(29)
	        			.addComponent(lblScience)
	        			.addContainerGap(204, Short.MAX_VALUE))
	        		.addGroup(jPanel1Layout.createSequentialGroup()
	        			.addGap(82)
	        			.addComponent(lblImg)
	        			.addContainerGap(92, Short.MAX_VALUE))
	        );
	        jPanel1Layout.setVerticalGroup(
	        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
	        		.addGroup(jPanel1Layout.createSequentialGroup()
	        			.addComponent(lblImg)
	        			.addGap(49)
	        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
	        				.addComponent(lblName)
	        				.addComponent(userName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
	        			.addGap(26)
	        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
	        				.addComponent(lblSport)
	        				.addComponent(comboSport, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
	        			.addGap(26)
	        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
	        				.addComponent(lblMusic)
	        				.addComponent(comboMusic, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
	        			.addGap(18)
	        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
	        				.addComponent(lblShow)
	        				.addComponent(comboShow, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
	        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
	        				.addGroup(jPanel1Layout.createSequentialGroup()
	        					.addGap(20)
	        					.addComponent(lblScience))
	        				.addGroup(jPanel1Layout.createSequentialGroup()
	        					.addGap(18)
	        					.addComponent(comboScience, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
	        			.addGap(50)
	        			.addComponent(btnAdd)
	        			.addContainerGap())
	        );
	        jPanel1.setLayout(jPanel1Layout);
	        jPanel2.setBorder(BorderFactory.createTitledBorder("Personas Registradas"));
	        jPanel2.setLayout(new BorderLayout());
	        dataTable.setModel(new DefaultTableModel(
	            new Object [][] {
	            },
	            new String [] {
	            }
	        ));
	        jScrollPane1.setViewportView(dataTable);
	        jPanel2.setBackground(new Color(153, 218, 182));
	        jPanel2.add(jScrollPane1, BorderLayout.CENTER);
	        btnReady = new JButton("Listo");
	        btnReady.setForeground(new Color(245, 255, 250));
	        btnReady.setBackground(new Color(32, 178, 170));
	        btnReady.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        	check();	
	        	}
	        });

	        GroupLayout layout = new GroupLayout(getContentPane());
	        layout.setHorizontalGroup(
	        	layout.createParallelGroup(Alignment.LEADING)
	        		.addGroup(layout.createSequentialGroup()
	        			.addContainerGap()
	        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
	        				.addGroup(layout.createSequentialGroup()
	        					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	        					.addPreferredGap(ComponentPlacement.RELATED)
	        					.addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)
	        					.addContainerGap())
	        				.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
	        					.addComponent(btnReady, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
	        					.addGap(37))))
	        );
	        layout.setVerticalGroup(
	        	layout.createParallelGroup(Alignment.LEADING)
	        		.addGroup(layout.createSequentialGroup()
	        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
	        				.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
	        					.addContainerGap()
	        					.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	        					.addGap(59))
	        				.addGroup(layout.createSequentialGroup()
	        					.addGap(21)
	        					.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	        					.addGap(18)))
	        			.addPreferredGap(ComponentPlacement.RELATED)
	        			.addComponent(btnReady, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
	        			.addContainerGap())
	        );
	        getContentPane().setLayout(layout);

	        pack();
	    }                       
	    private void loadDataToTable(ActionEvent evt) { 
	    	String name=userName.getText();
	    	Integer sport=(Integer) comboSport.getSelectedItem();
	    	Integer music = (Integer)comboMusic.getSelectedItem();
	    	Integer show= (Integer)comboShow.getSelectedItem();
	    	Integer science= (Integer)comboScience.getSelectedItem();
	    	if (!name.equals("")) {
	    		Object row[]= {name,sport,music,show,science};
		    	((DefaultTableModel)dataTable.getModel()).addRow(row);
		    	userName.setText("");
		    	persons.add(new Person(name,sport,music,show,science)); 

			}
	    	else {
	    		JOptionPane optionPane = new JOptionPane("El nombre de usuario no puede estar vacio", JOptionPane.ERROR_MESSAGE);    
	    		JDialog dialog = optionPane.createDialog("Error al ingresar datos");
	    		dialog.setAlwaysOnTop(true);
	    		dialog.setVisible(true);	
			}
	    }                                        

		private void check() {
			if ( persons.size()>2) {
    			if (persons.size()>5) {
    				Query consultData= new Query(persons){
    					@Override
    					public void dispose() {
    						this.setVisible(true);
    						super.dispose();
    					}
    				};
	        		consultData.setVisible(true);
	        		dispose();
					
				}else {
					Final result= new Final(persons,2){
						@Override
						public void dispose() {
							this.setVisible(true);
							super.dispose();
						}
					};
	        		result.setVisible(true);
	        		dispose();							
				}	
			}else {
				JOptionPane optionPane = new JOptionPane("Tiene que ingresar m�s personas para continuar", JOptionPane.ERROR_MESSAGE);    
	    		JDialog dialog = optionPane.createDialog("Error al ingresar datos");
	    		dialog.setAlwaysOnTop(true);
	    		dialog.setVisible(true);
			}
		}
	}