package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import model.Person;
import persistence.PersonsJSON;
import javax.swing.JLabel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class InputFile extends JFrame {
	  	private JButton btnReady;
	    private JPanel jPanel2;
	    private JScrollPane jScrollPane1;
	    private JTable dataTable;
	    private JToggleButton btnLoadData;
	    private ArrayList<Person> persons;
	    private static PersonsJSON data;
	    private JLabel lblNewLabel;
	    
	   
	   public InputFile() {
	    	getContentPane().setBackground(new Color(102, 205, 170));
	        initComponents();
	        this.setLocationRelativeTo(null);
	    }

	    @SuppressWarnings("unchecked")
	    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
	    private void initComponents() {
	    	persons= new ArrayList<Person>();
	        jPanel2 = new JPanel();
	        jScrollPane1 = new JScrollPane();
	        dataTable = new JTable();
	        btnLoadData = new JToggleButton();
	        btnLoadData.setForeground(new Color(245, 255, 250));
	        btnLoadData.setBackground(new Color(32, 178, 170));
	        btnLoadData.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	      		  JFileChooser seleccionador = new JFileChooser(); //opens a window with the system files
				    FileNameExtensionFilter filtroDeExtension = new FileNameExtensionFilter(
				    		"*.json", "JSON"); //only allowed file types 
				    seleccionador.setFileFilter(filtroDeExtension);
				  extractedData(seleccionador);
	        	}
	        });
	        btnReady = new JButton();
	        btnReady.setForeground(new Color(245, 255, 250));
	        btnReady.setBackground(new Color(32, 178, 170));
	        btnReady.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		check();
	        	}
	        });

	        
	        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	        addWindowListener(new WindowAdapter() {
	            public void windowOpened(WindowEvent evt) {
	                Tools.loadTable(evt, dataTable);
	            }
	        });
	        jPanel2.setBorder(BorderFactory.createTitledBorder("Personas Registradas"));
	        jPanel2.setBackground(new Color(153, 218, 182));
	        jPanel2.setLayout(new BorderLayout());

	        dataTable.setModel(new DefaultTableModel(
	            new Object [][] {

	            },
	            new String [] {
	            }
	        ));
	        jScrollPane1.setViewportView(dataTable);
	        jPanel2.add(jScrollPane1,BorderLayout.CENTER);
	        btnLoadData.setText("Cargar datos");
	        btnReady.setText("Listo");
	        lblNewLabel = new JLabel("");
	        lblNewLabel.setIcon(new ImageIcon("src/groups.png"));
	        GroupLayout layout = new GroupLayout(getContentPane());
	        layout.setHorizontalGroup(
	        	layout.createParallelGroup(Alignment.TRAILING)
	        		.addGroup(layout.createSequentialGroup()
	        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
	        				.addGroup(layout.createSequentialGroup()
	        					.addContainerGap()
	        					.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, 522, GroupLayout.PREFERRED_SIZE))
	        				.addGroup(layout.createSequentialGroup()
	        					.addGap(159)
	        					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
	        					.addGap(18)
	        					.addComponent(btnLoadData, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)))
	        			.addContainerGap(15, Short.MAX_VALUE))
	        		.addGroup(layout.createSequentialGroup()
	        			.addGap(0, 394, Short.MAX_VALUE)
	        			.addComponent(btnReady, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
	        			.addGap(24))
	        );
	        layout.setVerticalGroup(
	        	layout.createParallelGroup(Alignment.LEADING)
	        		.addGroup(layout.createSequentialGroup()
	        			.addContainerGap()
	        			.addGroup(layout.createParallelGroup(Alignment.TRAILING)
	        				.addComponent(btnLoadData)
	        				.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE))
	        			.addGap(18)
	        			.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, 256, GroupLayout.PREFERRED_SIZE)
	        			.addGap(27)
	        			.addComponent(btnReady)
	        			.addContainerGap(16, Short.MAX_VALUE))
	        );
	        getContentPane().setLayout(layout);
	        pack();
	    }                      

                             
	    private void extractedData(JFileChooser seleccionador) {
			int seleccion = seleccionador.showOpenDialog(getParent()); //necesary for the file window to open
			  
			 if(seleccion == JFileChooser.APPROVE_OPTION) { //The chosen file is saved
				 File archivoElegido = seleccionador.getSelectedFile();
				 data= new PersonsJSON();
				 data= data.readJSON(archivoElegido.getPath());
				 try {
						for (int i = 0; i < data.size(); i++) {
							Person user= data.get(i);
							persons.add(user);
							 Object row[]= {user.getName(),user.getInterestSport(),user.getInterestMusic(),user.getInterestShow(),user.getInterestScience()};
						       ((DefaultTableModel)dataTable.getModel()).addRow(row);
						}
						
					} catch (Exception e1) {

					}  
			 }
		} 
		private void check() {//check that the number of users entered is correct
			if ( persons.size()>2) {
    			if (persons.size()>5) {
    				Query consultData= new Query(persons){
    					@Override
    					public void dispose() {
    						this.setVisible(true);
    						super.dispose();
    					}
    				};
	        		consultData.setVisible(true);
	        		dispose();
					
				}else {
					Final result= new Final(persons,2){
						@Override
						public void dispose() {
							this.setVisible(true);
							super.dispose();
						}
					};
	        		result.setVisible(true);
	        		dispose();							
				}

			}else {
				JOptionPane optionPane = new JOptionPane("Tiene que ingresar m�s personas para continuar", JOptionPane.ERROR_MESSAGE);    
	    		JDialog dialog = optionPane.createDialog("Error al ingresar datos");
	    		dialog.setAlwaysOnTop(true);
	    		dialog.setVisible(true);
			}
		}
	}