package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class Menu {
	private JFrame frame;
	private JPanel panel, panel_1;
	private JLabel lbTitle;
	private JButton btnInputData, btnInputFile;
	private JLabel lblMessageQuery, lblImage;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Menu() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(102, 205, 170));
		frame.setBounds(100, 100, 467, 282);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Tools.centerSceen(frame);
		panel = new JPanel();
		panel.setBackground(new Color(102, 205, 170));
		panel.setBounds(0, 0, 449, 235);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		panel_1 = new JPanel();
		panel_1.setBackground(new Color(102, 205, 170));
		panel_1.setBounds(24, 13, 401, 209);
		panel_1.setBackground(new Color(153, 218, 182));
		panel.add(panel_1);
		panel_1.setLayout(null);
		lbTitle = new JLabel("Clustering Humano");
		lbTitle.setFont(new Font("Source Code Pro Semibold", Font.BOLD, 16));
		lbTitle.setBounds(109, 102, 182, 30);
		panel_1.add(lbTitle);
		btnInputData = new JButton("Manual");
		btnInputData.setForeground(new Color(245, 255, 250));
		btnInputData.setBackground(new Color(32, 178, 170));
		btnInputData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InputData input = new InputData() {
					@Override
					public void dispose() {
						this.setVisible(true);
						super.dispose();
					}
				};
				input.setVisible(true);
				frame.dispose();
			}
		});
		btnInputData.setBounds(52, 171, 85, 25);
		panel_1.add(btnInputData);
		btnInputFile = new JButton("Por archivo JSON");
		btnInputFile.setForeground(new Color(245, 255, 250));
		btnInputFile.setBackground(new Color(32, 178, 170));
		btnInputFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InputFile input = new InputFile() {
					@Override
					public void dispose() {
						this.setVisible(true);
						super.dispose();
					}
				};
				input.setVisible(true);
				frame.dispose();
			}
		});
		btnInputFile.setBounds(216, 171, 139, 25);
		panel_1.add(btnInputFile);
		lblMessageQuery = new JLabel("Seleccione el ingreso de datos:");
		lblMessageQuery.setBounds(102, 142, 209, 16);
		panel_1.add(lblMessageQuery);
		lblImage = new JLabel("");
		lblImage.setIcon(new ImageIcon("src/groups2.png"));
		lblImage.setBounds(146, 0, 105, 98);
		panel_1.add(lblImage);
	}
}