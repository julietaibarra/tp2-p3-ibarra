package view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class Tools {

	public static  void centerSceen(JFrame frame) {
		Toolkit miPantalla= Toolkit.getDefaultToolkit();//Almacena nuestro sistema nativo de ventana
		Dimension tamanoPantalla=miPantalla.getScreenSize();
		frame.setLocation(tamanoPantalla.width/4, tamanoPantalla.height/4);
	}
	
	public static void optionsBox(JComboBox<Integer> casilla,int inicio,  int limite) {
		for (int i = inicio; i <= limite; i++) {
			casilla.addItem(i);
		}
	}
	  public static void loadTable(WindowEvent evt, JTable dataTable) {                                  
	        DefaultTableModel modelo = (DefaultTableModel)dataTable.getModel();
	        modelo.addColumn("Nombre");
	        modelo.addColumn("Deporte");
	        modelo.addColumn("M�sica");
	        modelo.addColumn("Espectaculos");
	        modelo.addColumn("Ciencia");
	    }   
	
}
