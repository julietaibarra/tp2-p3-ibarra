package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import model.Person;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Query extends JFrame {
	
	private ArrayList<Person> persons;
	private JPanel panel;
	private JLabel lblMessage1;
	private JComboBox<Integer> optionsBox;
	private JButton btnContinue; 
	private JLabel lblMessageQuery;
	
	public Query(ArrayList<Person> personsInput) {
		getContentPane().setBackground(new Color(102, 205, 170));
		persons= new ArrayList<Person>();
		persons=personsInput;
		initialize();
	}

	private void initialize() {
		setBounds(100, 100, 450, 247);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		Tools.centerSceen(this);
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Grupos a realizar", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(12, 13, 408, 171);
		panel.setBackground(new Color(153, 218, 182));
		getContentPane().add(panel);
		panel.setLayout(null);
		lblMessage1 = new JLabel("La cantidad de personas ingresadas es "+persons.size());
		lblMessage1.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 17));
		lblMessage1.setBounds(12, 37, 336, 40);
		panel.add(lblMessage1);
		optionsBox = new JComboBox<Integer>();
		optionsBox.setBackground(new Color(255, 255, 255));
		optionsBox.setBounds(246, 80, 76, 22);
		Tools.optionsBox(optionsBox, 2, getPosiblesGroups());
		panel.add(optionsBox);
		btnContinue = new JButton("Continuar");
		btnContinue.setForeground(new Color(245, 255, 250));
		btnContinue.setBackground(new Color(32, 178, 170));
		btnContinue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Final result= new Final(persons,(int)optionsBox.getSelectedItem()){
					@Override
					public void dispose() {
						this.setVisible(true);
						super.dispose();
					}
				};
        		result.setVisible(true);
        		dispose();
			}
		});
		btnContinue.setBounds(155, 130, 97, 25);
		panel.add(btnContinue);
		lblMessageQuery = new JLabel("Elija la cantidad de grupos que desea");
		lblMessageQuery.setBounds(12, 79, 222, 25);
		panel.add(lblMessageQuery);
	}
	private int getPosiblesGroups() {
		int limit=0;
		if (persons.size()<=5 ) {
			limit=2;
		}
		if (persons.size()>5 && persons.size()<=8 ) {
			limit=3;
		}
		if (persons.size()>8 ) {
			limit=4;
		}
		return limit;
	}
}