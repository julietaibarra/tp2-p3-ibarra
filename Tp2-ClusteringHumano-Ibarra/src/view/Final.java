package view;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;


import controller.Clustering;
import model.Person;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Final extends JFrame{
	private ArrayList<Person> persons;
	private Clustering cluster;
	private int cantGroups;
	private HashSet<Set<Person>> results;
	private JButton btnStatistics, btnAveragesGraphic;
    private JLabel jLabel1, lbMessage1, lbGroup1, lbGroup2, lbResults, lbMessage2, lbGroup3, lbGroup4;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JTable jtbMatrix;
    private JFreeChart graphicInterests, averageGraphic;
	private DefaultCategoryDataset dataInterests,averageData;    
	private double[][] average;

	public Final(ArrayList<Person> input, int cant) {
		getContentPane().setBackground(new Color(102, 205, 170));
		persons= new ArrayList<Person>();
		persons=input;
		cantGroups=cant;
		initComponents();
		}
	  
    @SuppressWarnings("unchecked")                       
    private void initComponents() {
    	cluster= new Clustering(persons);
    	cluster.generateGroups(cantGroups);
    	results= new HashSet<>();
		results= cluster.returnGroup();
		average=cluster.averageSimilarity();
        jPanel1 = new JPanel();
        jScrollPane1 = new JScrollPane();
        jtbMatrix = new JTable();
        lbGroup1 = new JLabel();
        lbGroup1.setFont(new Font("Tahoma", Font.PLAIN, 9));
        lbGroup2 = new JLabel();
        lbGroup2.setFont(new Font("Tahoma", Font.PLAIN, 9));
        btnStatistics = new JButton();
        btnStatistics.setForeground(new Color(245, 255, 250));
        btnStatistics.setBackground(new Color(32, 178, 170));
        btnStatistics.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		ChartPanel panel = new ChartPanel(graphicInterests);
				JFrame ventana = new JFrame("Gr�fico de intereses");
				ventana.getContentPane().add(panel);
				ventana.pack();
				ventana.setVisible(true);
        	}
        });
        jLabel1 = new JLabel();
        jLabel1.setIcon(new ImageIcon("src/groups2.png"));
        lbMessage1 = new JLabel();
        lbResults = new JLabel();
        lbMessage2 = new JLabel();
        lbGroup3 = new JLabel();
        lbGroup3.setFont(new Font("Tahoma", Font.PLAIN, 9));
        lbGroup4 = new JLabel();
        lbGroup4.setFont(new Font("Tahoma", Font.PLAIN, 9));
        Tools.centerSceen(this);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jPanel1.setBorder(BorderFactory.createTitledBorder("Resultados finales"));
        jPanel1.setBackground(new Color(153, 218, 182));
        jtbMatrix.setModel(new DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Deporte", "M�sica", " Espectaculos", "Ciencia"
            }
        ));
        this.showMatrix(average);
        jScrollPane1.setViewportView(jtbMatrix);
        lbGroup1.setText("Grupo 1");
        lbGroup2.setText("Grupo 2");
        btnStatistics.setText("Gr\u00E1fico de los intereses");
        lbMessage1.setText("Los grupos generados fueron "+cantGroups);
        lbResults.setText(results.toString());
        lbMessage2.setText("Los promedios de similaridad son");
        lbGroup3.setText("Grupo 3");
        lbGroup4.setText("Grupo 4");
        btnAveragesGraphic = new JButton("Gr�fico promedios");
        btnAveragesGraphic.setForeground(new Color(245, 255, 250));
        btnAveragesGraphic.setBackground(new Color(32, 178, 170));
        btnAveragesGraphic.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		ChartPanel panel = new ChartPanel(averageGraphic);
				JFrame ventana = new JFrame("Gr�fico de los promedios");
				ventana.getContentPane().add(panel);
				ventana.pack();
				ventana.setVisible(true);
        	}
        });
        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1Layout.setHorizontalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGap(22)
        					.addComponent(jLabel1)
        					.addGap(31)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
        						.addComponent(lbResults)
        						.addComponent(lbMessage2, GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
        						.addComponent(lbMessage1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGap(36)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        						.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        							.addComponent(lbGroup1)
        							.addComponent(lbGroup2))
        						.addComponent(lbGroup3)
        						.addComponent(lbGroup4))
        					.addPreferredGap(ComponentPlacement.UNRELATED)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        						.addGroup(jPanel1Layout.createSequentialGroup()
        							.addComponent(btnAveragesGraphic)
        							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        							.addComponent(btnStatistics, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
        						.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 364, GroupLayout.PREFERRED_SIZE))))
        			.addContainerGap(54, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGap(16)
        					.addComponent(jLabel1))
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGap(27)
        					.addComponent(lbMessage1)
        					.addPreferredGap(ComponentPlacement.UNRELATED)
        					.addComponent(lbResults)
        					.addGap(18)
        					.addComponent(lbMessage2)))
        			.addPreferredGap(ComponentPlacement.RELATED, 18, GroupLayout.PREFERRED_SIZE)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
        					.addComponent(lbGroup1, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(lbGroup2)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(lbGroup3)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(lbGroup4))
        				.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)))
        			.addGap(36)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(btnAveragesGraphic)
        				.addComponent(btnStatistics))
        			.addGap(32))
        );
        jPanel1.setLayout(jPanel1Layout);
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );
        pack();
        dataInterests = new DefaultCategoryDataset();
        dataToChartInterests();
		graphicInterests = ChartFactory.createBarChart("Intereses de cada persona", 
				"Intereses", 
				"Indices de interes", 
				dataInterests, 
				PlotOrientation.VERTICAL, 
				true, //referencia de colores
				false,//referencia con el mouse
				false);//referncia url
		
		 averageData = new DefaultCategoryDataset();
	     dataToChartAverage();
		averageGraphic = ChartFactory.createBarChart("Intereses de cada grupo", 
					"Intereses", 
					"Indices de interes", 
					averageData, 
					PlotOrientation.VERTICAL, 
					true, 
					false,
					false);
    }

	private void dataToChartInterests() {
		for (Person person : persons) {
        	dataInterests.addValue(person.getInterestSport(), person.getName(), "Deportes");
    		dataInterests.addValue(person.getInterestMusic(), person.getName(), "M�sica");
    		dataInterests.addValue(person.getInterestShow(), person.getName(), "Espectaculos");
    		dataInterests.addValue(person.getInterestScience(), person.getName(), "Ciencia");
		}
	}

	private void dataToChartAverage() {
		for (int i = 0; i < average.length; i++) {
			averageData.addValue(average[i][0],"Grupo "+(i+1), "Deportes");
    		averageData.addValue(average[i][1],"Grupo "+(i+1), "M�sica");
    		averageData.addValue(average[i][2],"Grupo "+(i+1), "Espectaculos");
    		averageData.addValue(average[i][3],"Grupo "+(i+1), "Ciencia");
		}
	}
    public void showMatrix(double matrix[][]){
        DefaultTableModel model= (DefaultTableModel)jtbMatrix.getModel();
        model.setRowCount(matrix.length);
        model.setColumnCount(matrix[0].length);
        try {
             
        for (int i = 0; i <= matrix.length; i++) {
            for (int j = 0; j <matrix[0].length; j++) {
                jtbMatrix.setValueAt(matrix[i][j], i, j);
            } 
        }
        } catch (Exception e) {
       }  
    }     
}