# Trabajo práctico 2 de Programación 3 : Clustering Humano

## introducción
La aplicación tiene como objetivo identificar en **dos , tres o cuatro**  grupos personas dependiendo sus intereses personales. Tenemos una lista de personas, y para cada persona tenemos su nombre y los siguientes datos:

• interestSport= Interés por los deportes

• interestMusic= Interés por la música

• interestShow = Interés por las noticias del espectáculo

• interestScience = Interés por la ciencia

Cada uno de estos datos se expresa como un entero entre 1 y 5, siendo 1 el menor interés y 5 el máximo interés en el tema. El ındice de similaridad entre dos personas i y j se define como:

similaridad(I, J) = | sportI − sportJ | + | musicI − musicJ | + | showI − showJ | + | scienceI − scienceJ |.

Al obtener los grupos también se tiene los promedios de similaridad y se genera un gráfico de barra que detalla los intereses de cada personas.

## Implementación
Para el ingreso de datos  de la aplicación se puede realizar de dos maneras: 

- **manual:** se cargan los datos desde un formulario los cuales se puede tener una vista preliminar desde una tabla. Si se quiere ingresar un usuario sin nombre el sistema no permitirá avanzar con la ejecución del programa.

- **Por archivo:** se puede cargar los datos de los usuario mediante un archivo Json, estos como en la manera manual se pueden ver antes de ejecutar el algoritmo en una tabla.

Cuando los datos sean cargados independientemente de la manera en que fueron ingresados se verificará que la cantidad de usuario sea mayor que dos, en caso contrario no permitirá continuar.

Si la cantidad de personas es menor o igual a cinco, el sistema automáticamente generará solo dos grupos. Cuando la cantidad de personas supera cinco pero es inferior o igual a ocho, se tendrá la opción de generar dos o tres grupos.  Si la cantidad de usuarios es superior a ocho,  se podrán realizar dos , tres o cuatro grupos.

Los grupos generados se podrán visualizar  junto con su promedio de similaridad y un gráfico de los intereses.

## Paquetes
Para la implementación de esta aplicación se crearon los siguientes paquetes:
- **model:** contiene toda la lógica de la aplicación y sus respectivos test
- **view:** contiene la interfaz gráfica de la aplicación.
- **controller:** este paquete contiene la clase del algoritmo principal que recibe los datos ingresados ya sea de manera manual o por archivo JSON, y su respectivo test.
- **persistence:** este paquete tiene la clase que permite cargar los datos desde un archivo JSON.


## Descripcion de las clases de model

### Person
contiene los datos de las personas como su nombre y las calificaciones de intereses. Cumple la función de un vértice en el grafo. Esta clase tiene como variante de representación que :
Los nombre no pueden ser cadenas de caracteres vacíos
Las calificaciones de intereses tienen que ser números entre 1 a 5 inclusive. esta clase posee sus respectivos setter y getters
**métodos de la clase person** :Person solo contiene los métodos setter y getters 

### Edge
Edge o arista es la encargada de contener las relaciones entre personas y de *calcular su similaridad* entre ellas.

**similarity(Person p1, Person p2)** calcula la similaridad entre dos personas y ese resultados  queda almacena en el peso de la arista ( en la variable con el nombre de  length)
Además a clase Edge contiene  los métodos setters y getter correspondientes.

### Graph 
Graph o grafo es la estructura que contiene todas personas y las relaciones generadas en Edge. También contiene los vecinos que de los vértices(Person). Puede agregar personas por una o a través de un arraylist y agregar o eliminar aristas . También puede eliminarlas pero solo por una. Grafo contiene los siguientes métodos :

- **void addPersons(Person p):** Se  encarga  de agregar al grafo una persona. Esta función llama a las funciones a  **addToPerson(Person p)** que se encarga de agregar a dicha persona al arrayList de personas, y también llama , en el caso de que la cantidad de personas en el arrayLists sea diferente de cero , a la función a **addEdge(Person p1, Person p2)** que agrega las aristas al grafo.

- **void addEdge(Person p1, Person p2):** esta función agrega de a dos personas generando la arista correspondiente a la relación entre ambas. Verifica si no hay ciclo  y si la arista no existe en el grafo. También va agregando a las personas ingresadas sus vecinos correspondientes y va agregando las aristas formadas al arrayList llamado edges.

- **void addneighbor(Person p1, Person p2):** cuando se crea una arista, esta fusión va agregando los vecinos de la persona P1 en un HashMap<Person, HashSet<Person>> llamado neighbors. Esta HashMap genera una lista de vecinos. Que dando las relaciones de vecinos de la siguiente manera, siendo un grafo completo de tres vértices : {{p1,{p2,p3}}, {p2,{p1,p3}},{p3,{p1,p2}}}.

- **void removePerson(Person p):** elimina la persona pasada por parametro del grafo.

- **void removeEdge(Person p1,Person p2):** elimina la arista formada por las dos personas pasadas por parametro.

- **void removeNeighbor(Person key, Person p):** Sirve para quitar de los vecinos de la persona Key a la persona p.
Además de estos métodos se tienen los métodos getters  y los métodos que corroboran si la persona y la arista existe en el grafo.


### BFS
Contiene el algoritmo necesario para recorrer el grafo. Contiene los siguientes métodos :
boolean isConnectedGraph(Graph g): indica si un grafo es conexo.
- **Set<Person> achievable(Graph g, Person origin):** devuelve los vértices (en este caso persons )  alcanzables  que tiene la persona origin. 
- **void addPendingNeightbors(Graph g, Person i):** recorre los vecinos del vértice i y si no fue marcado y si no está en L se los agrega 
- **void inicialize(Graph g,Person origin):** inicializa el arrayList L con la persona origin e indica el índice de marked, que es igual a la cantidad de personas en el grafo.

### Kruskal
Es la clase  que contiene el algoritmo con el mismo nombre que  genera el árbol generador mínimo  Link indica la pertenecia la componente conexa
- **void initRoot()** :Inicializa las raíces.
- **int find(int i):** Devuelve a que componente conexa pertenece i, es decir su raíz.
- **void union(int i, int j):** a punta la raíz de j apunte a i.
- **Graph minimalGeneratorTree():** Esta función es la encargada de generar el árbol generador mínimo de la siguiente manera: Primero Ordena la lista de aristas (listEdge) y  guarda la cantidad de personas, que pasa a ser la cantidad de componentes conexas. Entonces mientras que la cantidad de componentes de componente conexa sea distinto de uno y la posición ( que inicialmente comienza en cero ) sea menor a la cantidad de arista en el grafo , tomó una arista de listEdge, que se llama current, y averigua si se puede unir componentes sin que se formen ciclos. Cuando termina, devuelve un nuevo grafo resultado de este algoritmo.


## Descripción de las clases de view

### Menu 
es la encargada de mostrar la primera ventana de la ejecución del algoritmo. 

### InputData
Permite el ingreso de datos manuales de los usuarios. Además de inicialize tiene la función *loadDataToTable* que amedida que se van cargando los datos se almacenen en la table.

### InputFile
Permite el ingreso de datos desde un archivo JSON. Esta clase tiene la función llamada **extractedData** que toma los datos ingresados en el archivo JSON y los carga en un tabla.

### Query
Consulta , en el caso de que la cantidad de personas ingresadas al sistema sea mayor a cinco, la cantidad de grupos que se desea generar. Esta clase tiene la función **getPosiblesGroups()** que tiene las posibles cantidades de grupos que se pueden generar dependiendo la canntidad de personas ingresadas al sistema.

### Final 
Muestra los resultados obtenidos al finalizar el algoritmo, como los promedios de similaridad y el gráfico de los intereses de las personas cargadas en el sistema usando las librerías jcommon y jfreechart.Esta clase tiene tres funciones:

 - **dataToChartInterests():** Carga los datos de los resultados de los grupos y guarda los indice de interes de cada persona en el gráfico.

 - **dataToChartAverage():** carga los datos de los promedios de itereses de cada grupo en el gráfico.
 
 - **showMatrix(double matrix[][]):** muestra los promedios en la tabla.

### Tools
contiene los métodos de centrar Pantalla (**centerScreen**) y opciones de ComboBox (**optionsBox**) y cargar tabla (**loadTable**) que son comunes en varias clases de este paquete.

## Descripción de las clases de controller

### Clustering:
Ejecuta los algoritmos de las clases BFS y Kruskal con los datos ingresados, también genera los promedios de similaridad.
los métodos de esta clase son:
- **void cluster:** elimina la arista de mayor peso del grafo aux.

- **void geenrateGroup(int cant):** Crea grupos dependiendo la cantidad que se eligió.

- **void AddGroup:** almacena los grupos en HashSet<Set<Person>> groups

- **Set<Person> getGroup():** Ejecuta BFS.achievable(aux, person) para obtener los grupos de la persona pasada por parámetros.

- **HashSet<Set<Person>> returnGroup():** devuelve todos los grupos formados.

- **double[][] averageSimilarity():** calcula los promedios de similaridad de todos los grupos y los almacena en una matriz. Estos promedios son redondeados hacia arriba para evitar tener un decimal periodico.

## Descripción de las clases de persistance
**PersonsJSON:** Esta clases permite cargar a las personas desde un archivo JSON usando la librería gson.

## Problemas y soluciones 
Los primeros problemas que surgieron fue en el momento de implementar el grafo . La solución fue considerar a las personas como  vértices y sus relacionadas como artistas. En estas últimas se calcula la similaridad entre ambas quedando el resultado como el peso de la arista. Para almacenar datos en el grafo se puede hacer por aristas y por personas que es la manera en la que se almacena cuando se ejecuta el algoritmo.

El segundo problema que se presentó fue al momento de crear las JTables. Para solucionar este problema se recurrió al IDE netbeans, en el cual era mucho más sencillo realizarlas y editarlas en función de las necesidades que se necesitaron( crear una tabla dinámica que a medida que se agreguen personas se vayan mostrando en la tabla, crear una tabla vacía que se llenara con los datos pasados desde un archivo JSON, y la última tabla que se genera para mostrar los promedios dependiendo de la cantidad de grupos realizados.

Finalmente, el otro problema fue que se noto que hubo métodos en las clases visuales que se repetían que son centrar Pantalla, check y option Box, para resolver este problema se creó la clase Tools que contiene dichos métodos, excepto check ya que  al pasarlo a la nueva clase no ejecutaba la función de cerrar la ventana cuando ya se realizó la acción deseada.
